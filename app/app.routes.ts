import { Routes, RouterModule } from '@angular/router';

import { RouteComponent1 } from './route.component1';
import { RouteComponent2 } from './route.component2';


const APP_ROUTES = [
  { path: '', redirectTo: '/routeComponent1', pathMatch: 'full' },
  { path: 'routeComponent1', component: RouteComponent1 },
  { path: 'routeComponent2', component: RouteComponent2 }
];

export const AppRoutingModule = RouterModule.forRoot(APP_ROUTES);
