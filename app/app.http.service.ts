import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/Rx';

@Injectable()
export class HTTPService{
  constructor(private http: Http){};

  getCurrentTime = function(){
    return this.http.get("http://date.jsontest.com")
                    .map( res => res.json() );
  };

  submitRequest = function(param){
    console.log(param);
    return this.http.post("http://validate.jsontest.com/?json=" + param)
             .map( res => res.json() );
  };
}
