import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpModule }    from '@angular/http';


import { AppComponent }   from './app.component';
import { DataListComponent } from './data-list.component';
import { RouteComponent1 } from './route.component1';
import { RouteComponent2 } from './route.component2';
import { HTTPComponent } from './app.http.component';

import { AppRoutingModule } from './app.routes';

@NgModule({
  imports:      [ BrowserModule,
                  FormsModule,
                  AppRoutingModule,
                  HttpModule
                ],
  declarations: [ AppComponent, DataListComponent, RouteComponent1, RouteComponent2, HTTPComponent ],
  bootstrap:    [ AppComponent ]
})
export class AppModule { }
