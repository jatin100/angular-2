import { Injectable } from '@angular/core';

import { DATALIST } from './mock-data-list';

@Injectable()
export class DataListService {
	getDataList = function(){
		return DATALIST;
	};
}
