import { Component, Input, Output, EventEmitter } from '@angular/core';
import { AppComponent } from './app.component';
@Component({
    selector: 'data-list',
    template: `
    <div>
        <h2>{{title}}</h2>
    </div>
	<input type="text" [(ngModel)]="curentList.name">
	<button (click)=showAlert();>Click</button>
	<h4>{{ input }}</h4>
  <h2>Value Pass from Parent to Child - Broadcast@input</h2>
  <p>{{ "Current clicked item :" + curentList.name }}</p>
    `,

})
export class DataListComponent {
    @Input() curentList: {name:""};
    @Output() emitter = new EventEmitter();

    title = 'Datalist component';


  	showAlert = function(){
  		alert(this.curentList.name);
  		console.log(this.input);
      this.emitter.emit(this.curentList);
  	}
}
