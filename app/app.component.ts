import { Component, Input } from '@angular/core';
import { DataListService } from './data-list.service';

@Component({
  selector: 'my-app',
  templateUrl: './app/app.component.html',
  styles: [
		   'h2 { color: red; }',
		   'h1,p { color: green; }'
		],
  providers: [DataListService],
})
export class AppComponent {
    message;
    data;
  	dataList;
    dataListName;
    curentList = {name:""};
    emitted = "";
    constructor(private dataListService: DataListService){
        this.message = "Angular 2 component";
		this.data = {
			list:[
				{name: 'Jatin', address: 'Bangalore'},
				{name: 'Mohit', address: 'Bangalore'},
				{name: 'Jatin', address: 'Bangalore'},
				{name: 'Mohit', address: 'Bangalore'}
			]
		};
		this.dataList = this.dataListService.getDataList();
    }

    bindToInput(name){
      this.curentList = name;
      console.log(name);
    };

    emittedFromChild(event){
      this.emitted = event;
    }
}
