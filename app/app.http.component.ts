import { Component } from '@angular/core';
import { HTTPService } from './app.http.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'http-service',
  template: `
    <button (click)="getRequest();">Get Request</button>
    <p>Output: {{getData}}</p>
    <button (click)="postRequest()">Post Request</button>
    <p>Output: {{postData}}</p>
  `,
  providers: [HTTPService]
})
export class HTTPComponent {
  private getData: string;
  private postData: string;
  constructor(private httpService: HTTPService){}
    getRequest = function(){
      this.getData = this.httpService.getCurrentTime()
           .subscribe(
             data => this.getData = JSON.stringify( data.time ),
             error => alert(error),
             () => console.log("Finished GET")
           );
     console.log(this.getData);
    };
    postRequest = function(){
      var json = JSON.stringify({var1: 1, var2: 'Two'});
      this.httpService.submitRequest(json)
                      .subscribe(
                        data => this.postData = JSON.stringify( data ),
                        error => alert(error),
                        () => console.log("Finished POST")
                      );
    };
}
